# %%

# import neccesary libraries

#   basic packages

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import warnings
import os
import random

#   img packages

from PIL import Image
from skimage.transform import resize

#   deep learning packages 

import tensorflow as tf
from keras.metrics import Precision
from tensorflow import keras
from keras import Sequential
from keras.layers import Dense, Flatten,Conv2D,Dropout,AveragePooling2D

#   mlops packages

import mlflow
import mlflow.pyfunc

# %%

# fixed variables, setup seeds and filter warnings messages

ROOT_DIR = '../'

tf.keras.utils.set_random_seed(42)
tf.random.set_seed(42)
random.seed(42)
np.random.seed(42)

warnings.filterwarnings('ignore')

# %%

# define main functions, extract from paper HelpCode section

def _fix_df(df):
    """Prepare the Data Frame to be readable
    """
    df_new = df.drop(['ID'], axis=0)
    df_new.columns = df_new.iloc[0,:]
    df_new.drop([np.nan], axis=0, inplace=True)
    df_new.columns.name = 'ID'
    return df_new

def read_clinical_data(abs_path='../'):
    """Return excel data as pandas Data Frame
    """
    df_od = pd.read_excel(abs_path + 'ClinicalData/patient_data_od.xlsx', index_col=[0])
    df_os = pd.read_excel(abs_path + 'ClinicalData/patient_data_os.xlsx', index_col=[0])
    return _fix_df(df=df_od), _fix_df(df=df_os)

# %%

# read clinical data

df_od, df_os = read_clinical_data('./')

# %%

# feature engineering

def create_token_id(df_sample,direction = 'OS'):
    # reminder that: OD -> Oculus Dexter(right eye) and OS -> Oculus Sinister (left eye)

    df_sample = df_sample.copy()
    df_sample = df_sample.reset_index().rename({'index':'token_id'},axis = 1)

    df_sample['token_id'] = df_sample['token_id'].apply(lambda x: 'RET' + x.replace('#','').strip() + direction+'.jpg') 
    df_sample['eye_side'] = direction

    return df_sample

# %%

# join both eyes

df_test_os = create_token_id(df_os,'OS')
df_test_od = create_token_id(df_od,'OD')

df_join_data = pd.concat([df_test_os,df_test_od])

# %%

# class availables
# - 0, healthy
# - 1, glaucoma
# - 2, suspicious

# based on the test #2 from paper experiments, remove suspicious class (2)

df_join_data['Diagnosis'] = df_join_data['Diagnosis'].replace(2,0)

# %%

# get X and y datasets

def x_and_y_dataset(lower_index,upper_index,eye_class = 'both'):

    if eye_class == 'both':
        df_sample = df_join_data.copy() # in this fuction, df_join_data is a global variable
    else:
        df_sample = df_join_data[df_join_data.eye_side == eye_class]

    list_to_tensor,list_not_available = [],[]

    for i, row in df_sample.iloc[lower_index:upper_index,:].iterrows():
        try:
            # resize img to 224 x 224 because, RESNET50 was trained with this configuration
            resize_sample = resize(np.array(Image.open("FundusImages/" + row['token_id'])), (224, 224))
            list_to_tensor.append(resize_sample)
        except:
            print(f'the sample {row["token_id"]} is not available')
            list_not_available.append(row["token_id"])
        
    X_set = np.array(list_to_tensor)
    y_set = df_join_data.iloc[lower_index:upper_index,:].Diagnosis

    return X_set,y_set

# plot eyes sample

def visualize_four_samples(X_set):
    _, ax = plt.subplots(2, 2, figsize=(10, 10))
    index = 0

    for i in range(2):
        for j in range(2):
            cell = ax[i][j]
            cell.set_xticks([])
            cell.set_yticks([])
            cell.grid(False)
            cell.imshow(X_set[index], cmap='gray')
            index += 1

    plt.show()

# %%

# extract 150 observations for train, 50 for test and 20 for validation - left eye

X_train, y_train = x_and_y_dataset(0,150,'OS')
X_test, y_test = x_and_y_dataset(150,200,'OS')
X_val, y_val = x_and_y_dataset(200,220,'OS')

# %%

# create a simple CNN-ANN

#   ANN - architecture

model_simple = keras.Sequential([
     Conv2D(32, (3, 3), activation='relu', padding='same', input_shape=(224, 224, 3)),
     Conv2D(64, (3, 3), activation='relu', strides=(2, 2)),
     Conv2D(128, (3, 3), activation='relu'),
     Dropout(0.5),
     Flatten(),
     # Fully connected layers
     Dense(128, activation='relu'),
     Dense(128, activation='relu'),
     Dense(1, activation='sigmoid') 
])

#   compile settings

model_simple.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

#   train model

model_simple.fit(X_train, y_train, epochs=100, validation_data = (X_val,y_val))

# %%
