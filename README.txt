# ML_PAPILA
## INTEGRANTES
* Jhoan Chiri Maldonado 100%
* Lizette Isabel Andrea Gemio Duran 100%
* Rodrigo Condori Quisbert 100%
* Federico Ramirez Vinaya 100%
## DESCRIPCION DEL PROYECTO
Proyecto PYTHON que hace uso de Tensorflow/Keras para entrenar un modelo de deep learning del proyecto **PAPILA: CONJUNTO DE DATOS CLÍNICOS DE AMBOS OJOS PARA EVALUACIÓN DE PACIENTES CON GLAUCOMA**

El proyecto cuenta con los siguientes notebooks:
- papila_data_preparation.ipynb: Preparacion de los datos y versionamiento con DVC
- papila_classification.ipynb: Entrenamiento del modelo de Deep Learning y MLFlow