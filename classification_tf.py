# %%

# import necessary libraries

import pandas as pd
import numpy as np
import requests
import json

# deep learning packages

from keras.models import Sequential
from keras.layers import Dense
from keras.utils import to_categorical
from tensorflow import keras

# machine learning packages

from sklearn.metrics import accuracy_score, precision_score, recall_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.preprocessing import MinMaxScaler
from sklearn.neural_network import MLPClassifier

# mlops packages

import mlflow
import mlflow.pyfunc

# %%

# read csv files

df_data = pd.read_csv('wine.data')

# %%

# define vars

var_names = ['class_y','Alcohol','Malic acid','Ash','Alcalinity of ash','Magnesium',
             'Total phenols','Flavanoids','Nonflavanoid phenols',
             'Proanthocyanins','Color intensity','Hue','diluted wines',
             'Proline']

df_data.columns = var_names

for var_name in var_names:
    df_data.rename({var_name:var_name.lower().replace(' ','_')}, axis=1, inplace=True)

# fix class_y

df_data['class_y'] = df_data['class_y'] - 1

# %%

# define X and y sets

X = df_data.iloc[:,1:].values
y = df_data.iloc[:,0].values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42, stratify=y)

# %%

# scale X sets

scale = MinMaxScaler()
scale.fit(X_train)

X_train = scale.transform(X_train)
X_test = scale.transform(X_test)

# %%

# redefine y set

y_train = to_categorical(y_train)
y_test = to_categorical(y_test)

# %%

# create a multiclass model

model = Sequential()

model.add(Dense(64, activation='relu', input_dim=X_train.shape[1]))
model.add(Dense(64, activation='relu'))
model.add(Dense(3, activation='softmax'))


model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])


model.fit(X_train, y_train, epochs=50, batch_size=32,validation_split=0.1)

# %%

# save model

model.save('vine_multiclass.h5')

# %%

# simple metrics

loss, accuracy = model.evaluate(X_test, y_test)
print(f'Loss: {loss}, Accuracy: {accuracy}')

# %%

# predict test

predict_y_class = model.predict(X_test)

# %%

# put y label on the same format

y_pred_class = np.argmax(y_test,axis = 1)
y_test_class = np.argmax(predict_y_class,axis = 1)

# %%

# evaluate metrics

report = classification_report(y_test_class, y_pred_class)

print(report)

# %%

# retreival modal 

reconstructed_model = keras.models.load_model("vine_multiclass.h5")

# %%

# retreival metrics

loss, accuracy = reconstructed_model.evaluate(X_test, y_test)
print(f'Loss: {loss}, Accuracy: {accuracy}')

# %%

# With sklearn packages

classifier = MLPClassifier(hidden_layer_sizes=(10, 5), activation='relu', solver='adam', random_state=42)
classifier.fit(X_train, y_train)

# %%

# predict with sklearn

y_pred_sk = np.argmax(classifier.predict(X_test),axis = 1)

# %%

# evaluate metrics with sklearn

report_sk = classification_report(y_test_class, y_pred_sk)

print(report_sk)

# %%

# save basic functions for track performace metrics model

def predict_on_test_data(model,X_test):

    y_pred = model.predict(X_test)

    return y_pred

def predict_prob_on_test_data(model,X_test):

    y_pred = model.predict_proba(X_test)

    return y_pred

def get_metrics(y_true, y_pred):

    acc = accuracy_score(y_true, y_pred)
    prec = precision_score(y_true, y_pred, average='macro')
    recall = recall_score(y_true, y_pred,average='macro')
    # print(prec,recall)
    return {'accuracy': round(acc, 2), 'precision': round(prec, 2), 'recall': round(recall, 2)}

def predict_evaluate(model, X_test, y_test):

  y_pred = predict_on_test_data(model, X_test)
 
  run_metrics = get_metrics(y_test, y_pred)

  return run_metrics

# %%

# save experiments

def create_experiment(experiment_name,run_name, run_metrics,model,run_params = None):

    # mlflow.set_tracking_uri("http://localhost:8080") # Descomentar esta línea si desea usar cualquier base de datos como sqlite como almacenamiento de back-end
    mlflow.set_experiment(experiment_name)

    with mlflow.start_run():

        if not run_params == None:
            for param in run_params:
                mlflow.log_param(param, run_params[param])

        for metric in run_metrics:
            mlflow.log_metric(metric, run_metrics[metric])

        mlflow.sklearn.log_model(model, "model")

        mlflow.set_tag("MLC sklearn",run_name)

    print('La ejecución: %s fue registrada en el experimento: %s' %(run_name, experiment_name))

# %%

# define hyperparameter params for train model

def tuned_model(depth, wide,activation_f,solver):
    classifier = MLPClassifier(hidden_layer_sizes=(depth, wide), activation=activation_f, solver=solver, random_state=42)
    classifier.fit(X_train, y_train)
    return classifier


# %%

# create an instance - experiment

experiment_name = "basic_MLPC"
run_name = "add_more_layers_4"

# hyperparameters

depht = 20
wide = 10
current_activation = 'relu'
current_optimazer = 'adam'

current_clasifier = tuned_model(depht,wide,current_activation,current_optimazer)

run_metrics = predict_evaluate(current_clasifier, X_test, y_test)
print(run_metrics)

create_experiment(experiment_name, 
                  run_name, 
                  run_metrics,
                  current_clasifier, 
                  run_params = {"hidden_layes":(depht,wide), 
                                "activation_function":current_activation,
                                "optimazer":current_optimazer,
                                "random_state":42} 
                 )

# %%

# inference by create and saved model

model_name = "champion_MLPC"
model_version = 1

model_retreival = mlflow.pyfunc.load_model(
    model_uri=f"models:/{model_name}/{model_version}"
)

# %%

# predict by get model and visualize metrics

y_pred_champion = model_retreival.predict(X_test)

get_metrics(y_test, y_pred_champion)

# %%

# register model

with mlflow.start_run(run_name="prueba") as run:
    result = mlflow.register_model(
        "runs:/f53ef170d5ec4224883dc0d4c03a669b/model",
        "MLPC_optimizado"
    )
    
# %%

# move stage test

client = mlflow.tracking.MlflowClient()
client.transition_model_version_stage(
    name="MLPC_optimizado",
    version=1,
    stage="Production"
)

# %%

# test request by API

#   extract a sample

sample = np.random.choice(X_test.shape[0], size=5, replace=False)
sample_array = X_test[sample]

inference_request = {
        "dataframe_records": sample_array.tolist()
}

endpoint = "http://localhost:1234/invocations"

#   request to model on end-point

response = requests.post(endpoint, json=inference_request)

dict_response = json.loads(response.text)

#   format predictions

list_fix_predictions = []
for pred in dict_response['predictions']:
    list_fix_predictions.append(pred.index(max(pred)))

print(f'the predictions for 5 obs is: {list_fix_predictions} class')

# %%
